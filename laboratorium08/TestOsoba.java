package pl.edu.uwm.wmii.PatrykLatacz.laboratorium08;
import pl.imiajd.latacz.Osoba2;
import pl.imiajd.latacz.Pracownik2;
import pl.imiajd.latacz.Student2;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class TestOsoba {
    public static void main(String[] args) {
        Osoba2[] ludzie = new Osoba2[2];

        String[] imiona = {"Jan", "Piotr"};
        ludzie[0] = new Pracownik2("Nowak", imiona,LocalDate.of(1980,2,12),2000, LocalDate.of(2010,5,10), false);

        String[] imiona1 = {"Marek", "Rafał"};
        ludzie[1] = new Student2("Kowalski", imiona1,LocalDate.of(1999,4,30),"it", 5.9, false);

        for (Osoba2 p : ludzie) {
            System.out.println("Nazwisko: "+p.getNazwisko() + " Imiona: "+ p.getFormattedImiona() +" Ur: "+p.getFormattedDate(p.getDataUrodzenia()) +" Płeć: "+p.get_Plec() + p.getOpis());
        }
    }
}


