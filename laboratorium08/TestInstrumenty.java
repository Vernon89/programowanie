package pl.edu.uwm.wmii.PatrykLatacz.laboratorium08;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import pl.imiajd.latacz.Instrument;
import pl.imiajd.latacz.Flet;
import pl.imiajd.latacz.Fortepian;
import pl.imiajd.latacz.Skrzypce;

public class TestInstrumenty {
    public static void main(String[] args)
    {
        ArrayList<Instrument> orkiestra = new ArrayList<>();
        orkiestra.add(new Flet("Philipp Hammig"));
        orkiestra.add(new Fortepian("Fazioli"));
        orkiestra.add(new Skrzypce("Carlo Giordano"));
        orkiestra.add(new Flet("Philipp Hammig"));
        orkiestra.add(new Fortepian("Fazioli"));

        for(Instrument p : orkiestra){
            System.out.println("Instrument: "+p.getClass().getSimpleName()+" " + p.toString() + " dzwiek: " + p.dzwiek());
        }
    }
}
