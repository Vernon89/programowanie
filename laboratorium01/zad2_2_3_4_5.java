package pl.edu.uwm.wmii.PatrykLatacz.laboratorium01;
import java.util.Scanner;

public class zad2_2_3_4_5 {
    public static void main(String[] args) {
        int n;
        Scanner in = new Scanner(System.in);

        System.out.println("Enter a number: ");
        n = in.nextInt();
        System.out.println("Enter " + n + " numbers: ");

        float[] tab = new float[n];
        for (int i=1; i<=n; i++) {
            tab[i - 1] = in.nextFloat();
        }

        _2(tab, n);
        _3(tab, n);
        _4(tab, n);
        _5(tab, n);
    }
    public static void _2(float [] tab, int n){
        float suma = 0;
        for (int i=0; i<n; i++){
            if (tab[i]>0){
                suma += tab[i];
            }
        }
        System.out.println("2) Doubled sum of positive nums: " +(2*suma));
    }
    public static void _3(float [] tab, int n) {
        int p = 0;
        int neg = 0;
        int z = 0;
        for (int i=0; i<n; i++) {
            if (tab[i] > 0) {
                p++;
            }
            if (tab[i] < 0) {
                neg++;
            }
            if (tab[i] == 0) {
                z++;
            }
        }
        System.out.println("3) The num of:\nnums>0: "+p+"\nnums<0: "+neg+"\nnums=0: "+z);
    }
    public static void _4(float[] tab, int n){
        float min;
        float max;
        for(int i=0; i<n; i++){
            min = tab[0];
            max = tab[0];
            for(int j=0; j<n-1; j++){
                if (tab[j+1] < min){
                    min = tab[j+1];
                }
                if (tab[j+1]>max){
                    max = tab[j+1];
                }
            }
        System.out.println("4)");
        System.out.println("Min: "+min);
        System.out.println("Max: "+max);
        return;
        }
    }
    public static void _5(float[] tab, int n){
        int count = 0;
        System.out.println("5)");
        for(int i=0; i<n; i++){
            if(tab[i] > 0){
                if (i+1 < n  && tab[i+1] > 0){
                    System.out.println("("+tab[i]+","+ tab[i+1] + ")");
                    count += 1;
                    i +=1;
                }
            }
        }
        System.out.println(count + " pairs");
    }
}