package pl.edu.uwm.wmii.PatrykLatacz.laboratorium01;
import java.util.Scanner;

public class zad1_2 {
    public static void main(String[] args) {
        int n;
        Scanner in = new Scanner(System.in);

        System.out.println("Enter a number: ");
        n = in.nextInt();
        System.out.println("Enter " + n + " numbers: ");

        int[] tab = new int[n];
        for (int i = 1; i <= n; i++) {
            tab[i-1] = in.nextInt();
        }
        display(tab,n);
    }
    public static void display(int [] tab, int n){
        for(int i = 1; i<n; i++)
        {
            System.out.print(tab[i] + ",");
        }
        System.out.print(tab[0]);
    }
}
