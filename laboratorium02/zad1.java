package pl.edu.uwm.wmii.PatrykLatacz.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad1 {
    public static void main(String [] args) {
        System.out.println("Input a number n: ");
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        if (n<1 || n>100){
            System.err.println("Wrong number");
            System.exit(1);
        }
        int [] tab = new int [n];
        Random random = new Random();
        for (int i=0; i<n; i++){
            tab[i] = random.nextInt(1999)-999;
            System.out.println(tab[i]);
        }

        int odd = 0;
        int even = 0;
        for (int i=0;i<n;i++){
            if(tab[i]%2 == 0){
                even++;
            }
            else{
                odd++;
            }
        }
        System.out.println("Even: "+even+". Odd: "+odd);

        int neg = 0;
        int p = 0;
        int z = 0;
        for (int i=0;i<n;i++){
            if(tab[i]<0){
                neg++;
            }
            else if(tab[i]>0){
                p++;
            }
            else{
                z++;
            }
        }
        System.out.println("Negative: "+neg+". Positive: "+p+". Zero: "+z);

        int max = tab[0];
        int count = 1;
        for (int i=1;i<n;i++){
            if(tab[i] > max){
                max = tab[i];
                count = 1;
            }
            else if(tab[i] == max){
                count++;
            }
        }
        System.out.println("Max ("+count+" times): "+max);

        int sum_neg = 0;
        int sum_p = 0;
        for (int i=0;i<n;i++){
            if(tab[i]<0){
                sum_neg += tab[i];
            }
            else{
                sum_p += tab[i];
            }
        }
        System.out.println("Sum of negative nums: "+sum_neg+". Sum of positive nums: "+sum_p);

        int length = 0;
        int max_length = 0;
        for (int i=0;i<n;i++){
            if(tab[i]>0){
                length++;
            }
            else{
                if(length > max_length){
                    max_length = length;
                }
                length = 0;
            }
        }
        System.out.println("Length: "+max_length);

        for (int i=0;i<n;i++){
            if(tab[i]<0){
                tab[i] = -1;
            }
            else if(tab[i]>0){
                tab[i] = 1;
            }
            System.out.println(tab[i]);
        }

        System.out.println("Left: ");
        int left = scanner.nextInt();
        if (1>left || n<left){
            System.err.println("Wrong number");
            System.exit(1);
        }
        System.out.println("Right: ");
        int right = scanner.nextInt();
        if (1>right || n<right){
            System.err.println("Wrong number");
            System.exit(1);
        }
        for (int j=0;j<left;j++) {
            System.out.println(tab[j]);
        }
        for (int i=right;i>=left;i--){
                System.out.println(tab[i]);
        }
        for (int k=right+1;k<n;k++){
            System.out.println(tab[k]);
        }
    }
}
