package pl.edu.uwm.wmii.PatrykLatacz.laboratorium04;

import java.util.Arrays;
import java.util.Scanner;

public class zad1e {
    public static int[] where(String str, String subStr){
        int atIndex = 0;
        int count = 0;
        int[] tab = new int[str.length()];

        while (atIndex != -1)
        {
            atIndex = str.indexOf(subStr, atIndex);

            if(atIndex != -1)
            {
                tab[atIndex] = atIndex;
                count++;
                atIndex += subStr.length();
            }
        }
        return tab;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Input string: ");
        String string = in.nextLine();
        System.out.println("Input substring: ");
        String substring = in.nextLine();

        System.out.println(string + ": " + substring  + " : " + Arrays.toString(where(string, substring)));
    }


}
