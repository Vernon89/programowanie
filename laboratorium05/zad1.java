package pl.edu.uwm.wmii.PatrykLatacz.laboratorium05;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Scanner;

public class zad1 {
    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> c = new ArrayList<>();
        c.addAll(a);
        c.addAll(b);
        return c;
    }
    public static void main(String[] args) {
        int[] ids = {1, 4, 9, 16};
        int[] ids2 = {9, 7, 4, 9, 11};

        ArrayList<Integer> a = new ArrayList<>();
        ArrayList<Integer> b = new ArrayList<>();

        for (int id: ids) {
            a.add(id);
        }
        for(int id: ids2) {
            b.add(id);
        }

        System.out.println(a);
        System.out.println(b);
        ArrayList<Integer> c = append(a, b);
        System.out.println(c);
    }
}