package pl.edu.uwm.wmii.PatrykŁatacz.laboratorium00;
import java.lang.*;

public class Main {

    public static void main(String[] args) {
        //zad2i3
        //test git send after change
        int suma = 0;
        int iloczyn = 1;
        for (int i = 1; i <= 10; i++) {
            System.out.println(i);
            suma += i;
            iloczyn *= i;
        }
        System.out.println(suma);
        System.out.println(iloczyn);

        //zad4
        int saldo = 1000;
        for (int i = 1; i <= 3; i++) {
            saldo *= 1.06;
            System.out.println("Saldo po " + i + " roku: " + saldo);
        }

        //zad5
        System.out.print(" ");
        for (int i = 0; i < 4; i++) {
            System.out.print('-');
        }
        System.out.print('\n');
        System.out.print("|Java|");
        System.out.print('\n');
        System.out.print(" ");
        for (int i = 0; i < 4; i++) {
            System.out.print('-');
        }
        System.out.print('\n');
        //zad6
        System.out.println("  /////");
        System.out.println(" +\"\"\"\"\"+");
        System.out.println("(| o o |)");
        System.out.println("  | ~ |");
        System.out.println(" | '-' |");
        System.out.println(" +-----+");
        //zad7
        System.out.println("***             ***************     *********************        ***************      *            *     *       *    ");
        System.out.println("*  *            *             *               *                  *              *      *          *      *      *     ");
        System.out.println("*    *          *             *               *                  *              *       *        *       *     *      ");
        System.out.println("*      *        *             *               *                  *              *        *      *        *    *       ");
        System.out.println("*        *      *             *               *                  *              *         *    *         *   *        ");
        System.out.println("*        *      ***************               *                  ****************          *  *          *  *         ");
        System.out.println("*       *       *             *               *                  * *                        *            * *          ");
        System.out.println("*      *        *             *               *                  *   *                      *            *   *        ");
        System.out.println("*     *         *             *               *                  *    *                     *            *    *       ");
        System.out.println("* **            *             *               *                  *     *                    *            *     *      ");
        System.out.println("*               *             *               *                  *      *                   *            *      *     ");
        System.out.println("*               *             *               *                  *       *                  *            *       *    ");
        System.out.println("*               *             *               *                  *        *                 *            *        *   ");

        //zad8
        System.out.println("   +   ");
        System.out.println("  + +   ");
        System.out.println(" +   +  ");
        System.out.println("+-----+");
        System.out.println("| .-. |");
        System.out.println("| | | |");
        System.out.println("+-+-+-+");

//        String zwierze = """     **   **
//                               *  * *  *
//                              (  O   O )          -------
//                              (    *   )        /  Hello
//                              (   ~~~  )      <    Junior  |
//                            (            )      \  Coder! /
//                            (___      ___)        ________""";
//        System.out.println(zwierze);
        //zad 10
        System.out.println("Agata");
        System.out.println("Marta");
        System.out.println("Beata");
        System.out.print("\n");
        //zad11
        System.out.println("Jest ....\n" +
                "Jest jeszcze taka miłość\n" +
                "ślepa bo widoczna\n" +
                "jak szczęśliwe nieszczęście\n" +
                "pół radość pół rozpacz\n" +
                "ile to trzeba wierzyć\n" +
                "milczeć cierpieć nie pytać\n" +
                "skakać jak osioł do skrzynki pocztowej\n" +
                "by dostać nic\n" +
                "za wszystko\n" +
                "\n" +
                "miej serce i nie patrz w serce\n" +
                "odstraszy cię kochać");

        System.out.print("\n");
        //zad12
        System.out.println("*   *   *   *   *   * ========================================");
        System.out.println("  *   *   *   *   *   ========================================");
        System.out.println("*   *   *   *   *   * ========================================");
        System.out.println("  *   *   *   *   *   ========================================");
        System.out.println("*   *   *   *   *   * ========================================");
        System.out.println("  *   *   *   *   *   ========================================");
        System.out.println("*   *   *   *   *   * ========================================");
        System.out.println("  *   *   *   *   *   ========================================");
        System.out.println("*   *   *   *   *   * ========================================");
        System.out.println("==============================================================");
        System.out.println("==============================================================");
        System.out.println("==============================================================");
        System.out.println("==============================================================");

    }
}
