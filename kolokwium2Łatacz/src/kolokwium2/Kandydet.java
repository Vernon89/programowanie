package pl.edu.uwm.wmii.PatrykŁatacz.kolokwium2;

import java.util.ArrayList;
import java.util.HashMap;


class Kandydat implements Cloneable, Comparable<Kandydat> {
    private String nazwa;
    private int wiek;
    private String wyksztalcony;
    private int doswiadczenie;

    public Kandydat(String nazwa, int wiek, String wyksztalcony, int latadoswiadczenia) {
        this.nazwa = nazwa;
        this.wiek = wiek;
        this.wyksztalcony = wyksztalcony;
        this.doswiadczenie = latadoswiadczenia;
    }


    public int compareTo(Kandydat other) {

        if (this.wyksztalcony.compareTo(other.wyksztalcony) != 0) {
            return this.wyksztalcony.compareTo(other.wyksztalcony);
        }

        if (this.wiek != other.wiek) {

            if (this.wiek < other.wiek)
                return -1;
            else
                return 1;

        }

        if (this.doswiadczenie < other.doswiadczenie)
            return -1;

        if (this.doswiadczenie > other.doswiadczenie)
            return 1;

        return 0;
    }

    public String getNazwa() {

        return nazwa;
    }

    public int getdoswiadczenie() {

        return doswiadczenie;
    }

    public int getWiek() {

        return wiek;
    }

    public String getWyksztalcony() {

        return wyksztalcony;
    }

    public static boolean Qualified(Kandydat k) {
        if(k.getdoswiadczenie() >= Rekrutacja.doswiadczenie) {
            return true;
        }
        return false;
    }

    public static HashMap<Integer, String> RecruitmentMap(ArrayList<Kandydat> klist) {
        HashMap<Integer, String>  map = new HashMap<>();
        for(Kandydat e : klist)
            if(Qualified(e))
                map.put(e.getdoswiadczenie(), e.getNazwa());
        return map;
    }

}
