package pl.edu.uwm.wmii.PatrykŁatacz.kolokwium2;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;



public class Main {

    public static void main(String[] args) {
        ArrayList<Kandydat> grupa = new ArrayList<>();
        grupa.add(new Kandydat("Łatacz Patryk", 21, "licencjat", 1));
        grupa.add(new Kandydat("Jan Pawel II", 25, "licencjat", 2));
        grupa.add(new Kandydat("Łukasz Samoń", 27, "mistrzowie", 5));
        grupa.add(new Kandydat("Zbigniew Stonoga", 27, "mistrzowie", 6));

        System.out.print("\nBefore:\n");
        for (Kandydat e : grupa) {
            System.out.printf("%s %d, %s, %d\n", e.getNazwa(), e.getWiek(), e.getWyksztalcony(), e.getdoswiadczenie());
        }

        Collections.sort(grupa);

        System.out.print("\nWrzesniej:\n");
        for (Kandydat e : grupa) {
            System.out.printf("%s %d, %s, %d\n", e.getNazwa(), e.getWiek(), e.getWyksztalcony(), e.getdoswiadczenie());
        }

        Rekrutacja.setDoswiadczenie();

        System.out.print("\nZakwfalifikowani:\n");
        for (Kandydat e : grupa) {
            System.out.println(e.getNazwa() + " " + e.getdoswiadczenie() + " : " + Qualified(e));
        }

        System.out.print("\nMapa:\n");
        HashMap<Integer, String> map = RecruitmentMap(grupa);
        for (Integer e : map.keySet()) {
            System.out.println(e + " " + map.get(e));
        }

    }


}